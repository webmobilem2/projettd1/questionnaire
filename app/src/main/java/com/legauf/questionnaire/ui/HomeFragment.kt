package com.legauf.questionnaire.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.legauf.questionnaire.R
import com.legauf.questionnaire.ui.compose.ComposeList
class HomeFragment : Fragment(), OnClickListener {
    lateinit var quizGen: Button
    lateinit var quizGame: Button
    lateinit var pokemonList: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_activity, container, false)
        quizGen = view.findViewById(R.id.buttonG)
        quizGame = view.findViewById(R.id.buttonJ)
        pokemonList = view.findViewById(R.id.buttonPokemon)
        quizGen.setOnClickListener(this)
        quizGame.setOnClickListener(this)
        pokemonList.setOnClickListener(this)
        return view
    }
    override fun onClick(view: View) {
        val clickedButton = view as Button
        when (clickedButton.id) {
            R.id.buttonG -> {
                val ft = activity?.supportFragmentManager?.beginTransaction()
                ft?.replace(R.id.fragment_placeholder, Questionnaire1Fragment.newInstance())
                ft?.commit()
            }
            R.id.buttonJ -> {
                val ft = activity?.supportFragmentManager?.beginTransaction()
                ft?.replace(R.id.fragment_placeholder, Questionnaire2Fragment.newInstance())
                ft?.commit()
            }
            R.id.buttonPokemon -> {
                val i = Intent(activity?.baseContext, ComposeList::class.java)
                startActivity(i)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            HomeFragment().apply{}
    }
}
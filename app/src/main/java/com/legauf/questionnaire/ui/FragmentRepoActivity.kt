package com.legauf.questionnaire.ui

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.legauf.questionnaire.R
import com.legauf.questionnaire.ui.preferences.SettingsFragment
import com.legauf.questionnaire.ui.profile.ProfileFragment

class FragmentRepoActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var drawer: DrawerLayout
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var navigationView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        // startiung activity
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_repo_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //setting drawer menu
        drawer = findViewById(R.id.drawer_layout)
        toggle =
            ActionBarDrawerToggle(this, drawer, null, R.string.drawer_open, R.string.drawer_close)
        drawer.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        navigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        //setting fragments
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_placeholder, HomeFragment.newInstance())
        ft.commit();
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_drawer_home -> {
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.remove(supportFragmentManager.fragments[0])
                ft.add(R.id.fragment_placeholder, HomeFragment.newInstance())
                ft.commit()
                return true
            }
            R.id.menu_drawer_activity1 -> {
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.replace(R.id.fragment_placeholder, Questionnaire1Fragment.newInstance())
                ft.commit()
                return true
            }
            R.id.menu_drawer_activity2 -> {
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.remove(supportFragmentManager.fragments[0])
                ft.add(R.id.fragment_placeholder, Questionnaire2Fragment.newInstance())
                ft.commit()
                return true
            }
        }
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }

        when (item.itemId) {
            R.id.menu_settings -> {
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.remove(supportFragmentManager.fragments[0])
                ft.add(R.id.fragment_placeholder, SettingsFragment.newInstance())
                ft.commit()
                return true
            }
            R.id.menu_profile -> {
                val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                ft.remove(supportFragmentManager.fragments[0])
                ft.add(R.id.fragment_placeholder, ProfileFragment.newInstance())
                ft.commit()
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}
package com.legauf.questionnaire.ui.compose

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.dp
import com.legauf.questionnaire.data.Pokemon
import com.legauf.questionnaire.data.PokemonDataProvider

class QuestionnaireComposeHome {
    @Composable
    fun QuestionnaireHomeContent(navigateToProfile: (Pokemon) -> Unit) {
        val pokemons = remember { PokemonDataProvider.pokemonLists }
        LazyColumn(
            contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
        ) {
            items(
                items = pokemons,
                itemContent = {
                    PokemonListItem(pokemon = it, navigateToProfile)
                })
        }
    }
}
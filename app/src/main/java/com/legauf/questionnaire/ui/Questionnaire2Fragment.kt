package com.legauf.questionnaire.ui

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import com.legauf.questionnaire.R
import com.legauf.questionnaire.data.QuestionAnswer

class Questionnaire2Fragment : Fragment(), View.OnClickListener {
    private lateinit var context: Context
    private lateinit var totalQuestionsTextView: TextView
    private var questionTextView: TextView? = null
    private lateinit var ansA: Button
    private lateinit var ansB: Button
    private lateinit var ansC: Button
    private lateinit var ansD: Button
    private lateinit var submitBtn: Button
    private lateinit var relativeLayout: RelativeLayout
    private var score = 0
    private var totalQuestion: Int = QuestionAnswer.question2.size
    private var currentQuestionIndex = 0
    private var selectedAnswer = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_main, container, false)
        relativeLayout = view.findViewById(R.id.relativeLayout)
        relativeLayout.setBackgroundResource(R.drawable.htllkpoquzxy_1242_2688)
        totalQuestionsTextView = view.findViewById(R.id.total_question)
        questionTextView = view.findViewById(R.id.question)
        ansA = view.findViewById(R.id.ans_A)
        ansB = view.findViewById(R.id.ans_B)
        ansC = view.findViewById(R.id.ans_C)
        ansD = view.findViewById(R.id.ans_D)
        submitBtn = view.findViewById(R.id.submit_btn)
        submitBtn.text = getString(R.string.submit_btn)
        ansA.setOnClickListener(this)
        ansB.setOnClickListener(this)
        ansC.setOnClickListener(this)
        ansD.setOnClickListener(this)
        submitBtn.setOnClickListener(this)
        totalQuestionsTextView.text =
            String.format(getString(R.string.number_of_questions), totalQuestion)
        loadNewQuestion()
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context = context
    }

    override fun onClick(view: View) {
        ansA.setBackgroundColor(Color.WHITE)
        ansB.setBackgroundColor(Color.WHITE)
        ansC.setBackgroundColor(Color.WHITE)
        ansD.setBackgroundColor(Color.WHITE)
        val clickedButton = view as Button
        if (clickedButton.id == R.id.submit_btn) {
            if (selectedAnswer == getString(QuestionAnswer.correctAnswers2[currentQuestionIndex])) {
                score++
            }
            currentQuestionIndex++
            loadNewQuestion()
        } else {
            //choices button clicked
            selectedAnswer = clickedButton.text.toString()
            clickedButton.setBackgroundColor(getColor(context, R.color.teal_800))
        }
    }

    private fun loadNewQuestion() {
        if (currentQuestionIndex == totalQuestion) {
            finishQuiz()
            return
        }
        questionTextView!!.text = getString(QuestionAnswer.question2[currentQuestionIndex])
        ansA.text = getString(QuestionAnswer.choices2[currentQuestionIndex][0])
        ansB.text = getString(QuestionAnswer.choices2[currentQuestionIndex][1])
        ansC.text = getString(QuestionAnswer.choices2[currentQuestionIndex][2])
        ansD.text = getString(QuestionAnswer.choices2[currentQuestionIndex][3])
    }

    private fun finishQuiz() {
        val passStatus = if (score > totalQuestion * 0.60) {
            getString(R.string.passed)
        } else {
            getString(R.string.failed)
        }
        AlertDialog.Builder(context)
            .setTitle(passStatus)
            .setMessage(String.format(getString(R.string.score), score, totalQuestion))
            .setPositiveButton(
                getString(R.string.restart)
            ) { _: DialogInterface?, _: Int -> restartQuiz() }
            .setCancelable(false)
            .show()
    }

    private fun restartQuiz() {
        score = 0
        currentQuestionIndex = 0
        loadNewQuestion()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            Questionnaire2Fragment().apply{}
    }
}
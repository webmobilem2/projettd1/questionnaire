package com.legauf.questionnaire.ui.profile

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.RatingBar
import androidx.fragment.app.Fragment
import com.legauf.questionnaire.R


class ProfileFragment : Fragment(), View.OnClickListener {

    private lateinit var context: Context
    private lateinit var datePicker: DatePicker
    private lateinit var nomEditText: EditText
    private lateinit var firstnameEditText: EditText
    private lateinit var ratingBar: RatingBar
    private lateinit var submitProfileButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        submitProfileButton = view.findViewById(R.id.submit_profile_button)
        submitProfileButton.setOnClickListener(this)

        // get preferences and load datas
        val settings: SharedPreferences = context.getSharedPreferences("profil", 0)

        // load birth date
        val anniversary_day: Int = settings.getInt("anniversary_day", 1)
        val anniversary_month: Int = settings.getInt("anniversary_month", 0)
        val anniversary_year: Int = settings.getInt("anniversary_year", 1999)
        datePicker = view.findViewById(R.id.anniversary_date_picker)
        datePicker.updateDate(anniversary_year, anniversary_month, anniversary_day)

        // load name
        val name: String? = settings.getString("name", "")
        nomEditText = view.findViewById(R.id.plain_text_nom_input)
        nomEditText.setText(name)

        // load firstname
        val firstname : String? = settings.getString("firstname", "")
        firstnameEditText = view.findViewById(R.id.plain_text_prenom_input)
        firstnameEditText.setText(firstname)

        // load ratingBar
        val rating: Float = settings.getFloat("rating", 0f)
        ratingBar = view.findViewById(R.id.ratingBar)
        ratingBar.rating = rating
        return view
    }

    override fun onClick(view: View) {
        val clickedButton = view as Button
        Log.d("DEBUG", "here 1")
        if (clickedButton.id == R.id.submit_profile_button) {
            Log.d("DEBUG", "here 2")
            val settings: SharedPreferences = context.getSharedPreferences("profil", 0)
            val editor = settings.edit()
            editor.putString("name", nomEditText.text.toString())
            editor.putString("firstname", firstnameEditText.text.toString())
            editor.putInt("anniversary_day", datePicker.dayOfMonth)
            editor.putInt("anniversary_month", datePicker.month)
            editor.putInt("anniversary_year", datePicker.year)
            editor.putFloat("rating", ratingBar.rating)
            editor.apply()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ProfileFragment().apply {}
    }
}
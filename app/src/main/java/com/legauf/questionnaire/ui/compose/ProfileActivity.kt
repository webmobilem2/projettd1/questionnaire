package com.legauf.questionnaire.ui.compose

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import com.legauf.questionnaire.data.Pokemon
import com.legauf.questionnaire.ui.compose.ui.theme.QuestionnaireTheme

class ProfileActivity : AppCompatActivity() {

    private val pokemon: Pokemon by lazy {
        intent?.getSerializableExtra(PUPPY_ID) as Pokemon
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            QuestionnaireTheme {
                ProfileScreen(pokemon = pokemon)
            }
        }
    }

    companion object {
        private const val PUPPY_ID = "puppy_id"
        fun newIntent(context: Context, pokemon: Pokemon) =
            Intent(context, ProfileActivity::class.java).apply {
                putExtra(PUPPY_ID, pokemon)
            }
    }
}
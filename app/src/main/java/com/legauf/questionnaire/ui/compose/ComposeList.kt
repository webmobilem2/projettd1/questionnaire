package com.legauf.questionnaire.ui.compose

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.legauf.questionnaire.data.Pokemon
import com.legauf.questionnaire.ui.FragmentRepoActivity
import com.legauf.questionnaire.ui.compose.ui.theme.QuestionnaireTheme

class ComposeList : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            QuestionnaireTheme {
                // A surface container using the 'background' color from the theme
                //Surface(
                //    modifier = Modifier.fillMaxSize(),
                //    color = MaterialTheme.colors.background
                //) {
                //    Greeting("Android")
                //}
                MyApp {
                    startActivity(ProfileActivity.newIntent(this, it))
                }
            }
        }
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    fun MyApp(navigateToProfile: (Pokemon) -> Unit) {
        Scaffold(
            content = {
                QuestionnaireComposeHome().QuestionnaireHomeContent(navigateToProfile = navigateToProfile)
            }
        )
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        startActivity(Intent(this, FragmentRepoActivity::class.java))
    }

}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    QuestionnaireTheme {
        Greeting("Android")
    }
}


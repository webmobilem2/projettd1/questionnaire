package com.legauf.questionnaire.ui.compose

import android.util.Log
import com.legauf.questionnaire.R
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.legauf.questionnaire.data.Pokemon

@Composable
fun ProfileScreen(pokemon: Pokemon) {
    val scrollState = rememberScrollState()

    Column(modifier = Modifier.fillMaxSize()) {
        BoxWithConstraints {
            Surface {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(scrollState),
                ) {
                    ProfileHeader(
                        pokemon,
                        this@BoxWithConstraints.maxHeight
                    )
                    ProfileContent(
                        pokemon,
                        this@BoxWithConstraints.maxHeight)
                }
            }
        }
    }
}

@Composable
private fun ProfileHeader(
    pokemon: Pokemon,
    containerHeight: Dp
) {
    Image(
        modifier = Modifier
            .heightIn(max = containerHeight / 2)
            .fillMaxWidth(),
        painter = painterResource(id = pokemon.pokemonImageId),
        contentScale = ContentScale.Crop,
        contentDescription = null
    )
}

@Composable
private fun ProfileContent(pokemon: Pokemon, containerHeight: Dp) {
    Column {
        Title(pokemon)
        Log.d("debug pokemon", pokemon.toString())
        ProfileProperty(LocalContext.current.getString(R.string.name), LocalContext.current.getString(pokemon.title), "")
        ProfileProperty(LocalContext.current.getString(R.string.type), LocalContext.current.getString(pokemon.type), "")
        ProfileProperty(LocalContext.current.getString(R.string.size), LocalContext.current.getString(pokemon.size), LocalContext.current.getString(R.string.size_param))
        ProfileProperty(LocalContext.current.getString(R.string.poids), LocalContext.current.getString(pokemon.poids), LocalContext.current.getString(R.string.poids_param))
        ProfileProperty(LocalContext.current.getString(R.string.talent), LocalContext.current.getString(pokemon.talent), "")
        Spacer(Modifier.height((containerHeight - 460.dp).coerceAtLeast(0.dp)))
    }
}

@Composable
private fun Title(
    pokemon: Pokemon
) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp, top = 16.dp)) {
        Text(
            text = LocalContext.current.getString(pokemon.title),
            style = MaterialTheme.typography.h5,
            fontWeight = FontWeight.Bold
        )
    }
}

@Composable
fun ProfileProperty(label: String, value: String, supplement: String) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Divider(modifier = Modifier.padding(bottom = 4.dp))
        Text(
            text = label,
            modifier = Modifier.height(24.dp),
            style = MaterialTheme.typography.caption,
        )
        Text(
            text = value + supplement,
            modifier = Modifier.height(24.dp),
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Visible
        )
    }
}
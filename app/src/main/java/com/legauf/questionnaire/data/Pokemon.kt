package com.legauf.questionnaire.data

import java.io.Serializable

data class Pokemon(
    val id: Int,
    val title: Int,
    val type: Int,
    val size: Int,
    val poids: Int,
    val talent: Int,
    val pokemonImageId: Int = 0
) : Serializable
package com.legauf.questionnaire.data

import com.legauf.questionnaire.R

object PokemonDataProvider {

    val pokemonLists = listOf(
        Pokemon(
            id = 1,
            title = R.string.pokemon_title_1,
            type = R.string.pokemon_type_plante,
            size = R.string.pokemon_size_1,
            poids = R.string.pokemon_poids_1,
            talent = R.string.pokemon_talent_1,
            pokemonImageId = R.drawable.p1
        ),
        Pokemon(
            id = 2,
            title = R.string.pokemon_title_2,
            type = R.string.pokemon_type_plante,
            size = R.string.pokemon_size_2,
            poids = R.string.pokemon_poids_2,
            talent = R.string.pokemon_talent_2,
            pokemonImageId = R.drawable.p2
        ),
        Pokemon(
            id = 3,
            title = R.string.pokemon_title_3,
            type = R.string.pokemon_type_plante,
            size = R.string.pokemon_size_3,
            poids = R.string.pokemon_poids_3,
            talent = R.string.pokemon_talent_3,
            pokemonImageId = R.drawable.p3
        ),
        Pokemon(
            id = 4,
            title = R.string.pokemon_title_4,
            type = R.string.pokemon_type_feu,
            size = R.string.pokemon_size_4,
            poids = R.string.pokemon_poids_4,
            talent = R.string.pokemon_talent_4,
            pokemonImageId = R.drawable.p4
        ),
        Pokemon(
            id = 5,
            title = R.string.pokemon_title_5,
            type = R.string.pokemon_type_feu,
            size = R.string.pokemon_size_5,
            poids = R.string.pokemon_poids_5,
            talent = R.string.pokemon_talent_5,
            pokemonImageId = R.drawable.p5
        ),
        Pokemon(
            id = 6,
            title = R.string.pokemon_title_6,
            type = R.string.pokemon_type_feu,
            size = R.string.pokemon_size_6,
            poids = R.string.pokemon_poids_6,
            talent = R.string.pokemon_talent_6,
            pokemonImageId = R.drawable.p6
        ),
        Pokemon(
            id = 7,
            title = R.string.pokemon_title_7,
            type = R.string.pokemon_type_eau,
            size = R.string.pokemon_size_7,
            poids = R.string.pokemon_poids_7,
            talent = R.string.pokemon_talent_7,
            pokemonImageId = R.drawable.p7
        ),
        Pokemon(
            id = 8,
            title = R.string.pokemon_title_8,
            type = R.string.pokemon_type_eau,
            size = R.string.pokemon_size_8,
            poids = R.string.pokemon_poids_8,
            talent = R.string.pokemon_talent_8,
            pokemonImageId = R.drawable.p8
        ),
        Pokemon(
            id = 9,
            title = R.string.pokemon_title_9,
            type = R.string.pokemon_type_eau,
            size = R.string.pokemon_size_9,
            poids = R.string.pokemon_poids_9,
            talent = R.string.pokemon_talent_9,
            pokemonImageId = R.drawable.p9
        ),
        Pokemon(
            id = 10,
            title = R.string.pokemon_title_10,
            type = R.string.pokemon_type_normal,
            size = R.string.pokemon_size_10,
            poids = R.string.pokemon_poids_10,
            talent = R.string.pokemon_talent_10,
            pokemonImageId = R.drawable.p10
        ),
        Pokemon(
            id = 11,
            title = R.string.pokemon_title_11,
            type = R.string.pokemon_type_normal,
            size = R.string.pokemon_size_11,
            poids = R.string.pokemon_poids_11,
            talent = R.string.pokemon_talent_11,
            pokemonImageId = R.drawable.p11
        ),
        Pokemon(
            id = 12,
            title = R.string.pokemon_title_12,
            type = R.string.pokemon_type_insecte,
            size = R.string.pokemon_size_12,
            poids = R.string.pokemon_poids_12,
            talent = R.string.pokemon_talent_12,
            pokemonImageId = R.drawable.p12
        ),
        Pokemon(
            id = 13,
            title = R.string.pokemon_title_13,
            type = R.string.pokemon_type_insecte,
            size = R.string.pokemon_size_13,
            poids = R.string.pokemon_poids_13,
            talent = R.string.pokemon_talent_13,
            pokemonImageId = R.drawable.p13
        ),
    )
}
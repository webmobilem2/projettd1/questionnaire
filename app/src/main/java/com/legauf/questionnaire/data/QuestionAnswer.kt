package com.legauf.questionnaire.data

import com.legauf.questionnaire.R

class QuestionAnswer {
    companion object {
        val question = arrayOf(
            R.string.question_one,
            R.string.question_two,
            R.string.question_three,
            R.string.question_four
        )

        val choices = arrayOf(
            arrayOf(
                R.string.choice_1_1,
                R.string.choice_1_2,
                R.string.choice_1_3,
                R.string.choice_1_4
            ),
            arrayOf(
                R.string.choice_2_1,
                R.string.choice_2_2,
                R.string.choice_2_3,
                R.string.choice_2_4
            ),
            arrayOf(
                R.string.choice_3_1,
                R.string.choice_3_2,
                R.string.choice_3_3,
                R.string.choice_3_4
            ),
            arrayOf(
                R.string.choice_4_1,
                R.string.choice_4_2,
                R.string.choice_4_3,
                R.string.choice_4_4
            )
        )

        val correctAnswers = arrayOf(
            R.string.choice_1_1,
            R.string.choice_2_3,
            R.string.choice_3_4,
            R.string.choice_4_2
        )

        val question2 = arrayOf(
            R.string.question2_one,
            R.string.question2_two,
            R.string.question2_three,
            R.string.question2_four
        )

        val choices2 = arrayOf(
            arrayOf(
                R.string.choice2_1_1,
                R.string.choice2_1_2,
                R.string.choice2_1_3,
                R.string.choice2_1_4
            ),
            arrayOf(
                R.string.choice2_2_1,
                R.string.choice2_2_2,
                R.string.choice2_2_3,
                R.string.choice2_2_4
            ),
            arrayOf(
                R.string.choice2_3_1,
                R.string.choice2_3_2,
                R.string.choice2_3_3,
                R.string.choice2_3_4
            ),
            arrayOf(
                R.string.choice2_4_1,
                R.string.choice2_4_2,
                R.string.choice2_4_3,
                R.string.choice2_4_4
            )
        )

        val correctAnswers2 = arrayOf(
            R.string.choice2_1_3,
            R.string.choice2_2_1,
            R.string.choice2_3_1,
            R.string.choice2_4_4
        )
    }

}
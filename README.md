[![OS](https://badgen.net/badge/OS/Android?icon=https://raw.githubusercontent.com/androiddevnotes/awesome-android-kotlin-apps/master/assets/android.svg&color=3ddc84)]()
[![Kotlin](https://img.shields.io/badge/Kotlin-1.7.21-blue.svg)](http://kotlinlang.org)
[![Language](https://img.shields.io/github/languages/top/cortinico/kotlin-android-template?color=blue&logo=kotlin)]()
[![pipeline status](https://gitlab.com/webmobilem2/projettd1/questionnaire/badges/main/pipeline.svg)](https://gitlab.com/webmobilem2/projettd1/questionnaire/commits/main)

![GIF demo](app/src/main/device-2023-02-09-120857.gif)
_Gif du splashscreen de l'application 09/02/23_

# Version

Version **1.0** de l'application

# Table of Contents
[[_TOC_]]

# Questionnaire
Cette application est un questionnaire actuellement de quatres questions pour tester tes connaissances

## Description
Cette application a été réalisé dans le cadre d'un projet d'école au campus Ynov Aix.  
Le but de l'application est d'apprendre à développer des applications pour Android natif au travers de la création d'un questionnaire.   

## Getting Started

Il s'agit d'un projet _Android_ créé avec le langage _Kotlin_ sur _Android Studio_.
L'api _33_ d'android est utilisée sur ce projet

### Prérequis

Installer [Android Studio](https://developer.android.com/studio).

### Installation

1. Cloner le repo
   ```sh
   git clone https://gitlab.com/webmobilem2/projettd1/questionnaire.git
   ```
2. Dans _Android Studio_, sélectionner _Open an Existing Project_ puis effectuer une compilation
OU compiler soi-même l'application avec Gradle

3. Voilà ! Vous pouvez maintenant explorer le projet comme bon vous semble.

## Utilisation basique

![GIF demo](app/src/main/device-2023-02-09-120755.gif)
_Gif de présentation du projet 09/02/23_

## Bonnes Pratiques

  * Séparation packages
  * Pas de hardcoded string
  * Mise en place d'une CI/CD
  * Ajouts de tests unitaires
  * Architecture MVVM

## Dependencies

### Libraries
  * AndroidX
  * Lottie : 5.2.0
  * Junit

## Roadmap

* Amélioration questionnaire et contenu
* Ajout d'un lien vers page wiki(ou non) en accompagnement de la réponse
* Publication de l'application sur le Store

## Contributors
- LEMARCHAND Enzo
- MARGAIL Maurin